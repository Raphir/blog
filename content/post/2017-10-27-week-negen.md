---
title: Plaatsen van een nieuwe blogpost.
subtitle: via Atom
date: 2017-10-27
tags: ["blog"]
----

# Week 9
## Gezamenlijk
We begonnen aan het kijken wat er aan het ontwerp en dergelijke moest verbeterd worden.
Wat het spel nog leuker en interessanter kon maken voor beginnende CMD leerlingen.
Hierna begon de laatste iteratie en kregen we vaak peerfeedback.
Dit was voor ons belangrijk, want zo konden we nog zien hoe we het spel echt leuker konden maken,
met bijvoorbeeld met architectuur, of meer spanning door middel van een grotere map.

## Individueel
De samenwerking verliep weer goed en ik heb veel geleerd deze iteraties.
