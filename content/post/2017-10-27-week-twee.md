---
title: Plaatsen van een nieuwe blogpost.
subtitle: via Atom
date: 2017-10-27
tags: ["blog"]
----

# Week 2
## Gezamenlijk
In week 2 waren de teams zo ongeveer definitief gevormd
Ons team was al definitief, maar van velen nog niet.
We begonnen gelijk met een spel, waarbij je een icoon moest namaken.
Wij kozen voor the Notorious B.i.G, omdat het voor ons allemaal een icoon was.
Daarna zijn we verder gegaan met de naam Notorious, omdat dit ons team zo goed mogelijk representeerd.
We begonnen daarna aan het brainstormen over ons idee en kwamen met verschillende
ideeēn en inbreng. Zo vulde iedereen elkaar aan en kwamen we op een gezamelijk idee.
Ons concept was daarom ook een quiz over verschillende gebouwen in Rotterdam, zodat
de nieuwe eerste jaar cmd-studenten ook Rotterdam leren kennen.
Hierna begonnen we aan een planning, wie wat zou doen, en dus een taakverdeling.
Als laatste gingen we onderzoek doen naar een doelgroep, en deden we interviews, wat CMD leerlingen
nou echt leuk vonden.
We pasten al deze technieken aan, aan ons concept.

## Individueel
Ik ging kijken naar mijn beste skills en toepassen waar het nodig was.
Ik deed bijvoorbeeld actief mee met het brainstormen.
Onze gezamenlijke doelen waren dat iedereen op tijd zou komen en dat we er
100% er voor zouden gaan als team. Teamafspraken waren helder en er werd keurig aan gehouden.
Er gingen dingen mis, zoals te laat komen of soms te laat reageren in de groeps app, maar
niet dat het fataal werd.
